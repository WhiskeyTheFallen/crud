--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6
-- Dumped by pg_dump version 10.6

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: bitacoras; Type: TABLE; Schema: public; Owner: ulises
--

CREATE TABLE public.bitacoras (
    id integer NOT NULL,
    id_usuario integer NOT NULL,
    accion character varying(255) NOT NULL,
    fecha timestamp without time zone NOT NULL
);


ALTER TABLE public.bitacoras OWNER TO ulises;

--
-- Name: bitacoras_id_seq; Type: SEQUENCE; Schema: public; Owner: ulises
--

CREATE SEQUENCE public.bitacoras_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bitacoras_id_seq OWNER TO ulises;

--
-- Name: bitacoras_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ulises
--

ALTER SEQUENCE public.bitacoras_id_seq OWNED BY public.bitacoras.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: ulises
--

CREATE TABLE public.users (
    id integer NOT NULL,
    correo character varying(255) NOT NULL,
    contrasena character varying(255) NOT NULL,
    nombre character varying(255) NOT NULL,
    primer_apellido character varying(50) NOT NULL,
    segundo_apellido character varying(50),
    habilitado boolean NOT NULL,
    administrador boolean NOT NULL,
    imagen character varying(255),
    imagen_dir character varying(255)
);


ALTER TABLE public.users OWNER TO ulises;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: ulises
--

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO ulises;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ulises
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: bitacoras id; Type: DEFAULT; Schema: public; Owner: ulises
--

ALTER TABLE ONLY public.bitacoras ALTER COLUMN id SET DEFAULT nextval('public.bitacoras_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: ulises
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: bitacoras; Type: TABLE DATA; Schema: public; Owner: ulises
--

COPY public.bitacoras (id, id_usuario, accion, fecha) FROM stdin;
1	49	Inicio de sesión	2019-01-25 17:52:22
2	49	Inicio de sesión	2019-01-25 17:57:21
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: ulises
--

COPY public.users (id, correo, contrasena, nombre, primer_apellido, segundo_apellido, habilitado, administrador, imagen, imagen_dir) FROM stdin;
49	uhulisesh@gmail.com	$2y$10$dAh6LBsrejmdIby83bb/8.2.pfjjrvkr9l1ZrsPdHjvFYrxnuKs1y	s	s		t	t	\N	\N
21	s11s@s.com	$2y$10$vKe4tjuKt.G6zAQpHat.9eghzJtYIo0ntV9wa//jcJdkXsbY/fPby	s	s		t	t	\N	\N
22	s@ss.coms	$2y$10$VWjbLQ9/PlLvUyjPWkd3k.yuNoHnzbh/bbRVKI6wEV.zb3b4hjU4a	s	s		t	t	\N	\N
23	aaass@s.com	$2y$10$BLxF4EFF4mTEPVKAowfo4e9Yj/FqYWCZWlXTpVz6.bNX/23wICm3a	a	s	s	t	t	\N	\N
25	uhuliszesh@gmail.com	$2y$10$F4ctdRqwkJjXPjnob5c.HuiAmTOoMPD01B77H3FdS/UYRW7IIWCUe	a	ss	h	f	f	\N	\N
\.


--
-- Name: bitacoras_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ulises
--

SELECT pg_catalog.setval('public.bitacoras_id_seq', 2, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ulises
--

SELECT pg_catalog.setval('public.users_id_seq', 49, true);


--
-- Name: bitacoras bitacoras_pkey; Type: CONSTRAINT; Schema: public; Owner: ulises
--

ALTER TABLE ONLY public.bitacoras
    ADD CONSTRAINT bitacoras_pkey PRIMARY KEY (id);


--
-- Name: users users_correo_key; Type: CONSTRAINT; Schema: public; Owner: ulises
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_correo_key UNIQUE (correo);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: ulises
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: bitacoras fk_bitacora; Type: FK CONSTRAINT; Schema: public; Owner: ulises
--

ALTER TABLE ONLY public.bitacoras
    ADD CONSTRAINT fk_bitacora FOREIGN KEY (id_usuario) REFERENCES public.users(id);


--
-- PostgreSQL database dump complete
--

