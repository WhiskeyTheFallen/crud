--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6
-- Dumped by pg_dump version 10.6

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: users; Type: TABLE; Schema: public; Owner: ulises
--

CREATE TABLE public.users (
    id integer NOT NULL,
    correo character varying(255) NOT NULL,
    contrasena character varying(255) NOT NULL,
    nombre character varying(255) NOT NULL,
    apellido_materno character varying(50) NOT NULL,
    apellido_paterno character varying(50),
    habilitado boolean NOT NULL,
    administrador boolean NOT NULL
);


ALTER TABLE public.users OWNER TO ulises;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: ulises
--

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO ulises;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ulises
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: ulises
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: ulises
--

COPY public.users (id, correo, contrasena, nombre, apellido_materno, apellido_paterno, habilitado, administrador) FROM stdin;
11	s11@s.com	$2y$10$l55bL/5dOpTL/6ARLMH6SugwYPYmqiyUtslKb0cAEkczNnMGvJKiu	s	s		t	f
8	s@s.coms	$2y$10$rpnWNGqBLJmQLoPpcOGm8uXbWtQdzRZ7UxANyE5EHNGD7/Gzv5iaW	s	s	s	t	t
7	s@s.com	$2y$10$LLSjr7.KEeglMUiGX.ATSevq7Vq.Ky0zBzHGpugY9kwqSTGG1hjR6	s	s		t	f
1	uhu@gmail.com	$2y$10$5nrwS3LOxsYLDricKJDOje5ujnMPKgkEnhB1bHNh34QnrtR9qOC1G	uhuu	huh		t	f
9	uhua@gmail.com	$2y$10$HyjsvK81IVcIFp1t8/OvsuM/zUZtk6BGodmD9hQVRMiT9asrb3IWm	a	a		t	t
10	as@s.com	$2y$10$L6ju6geiUgnOVz9SMLN4DehluaIN5tE36Z8IpPP69RxKpgihVOYV6	s	s		t	t
12	aaas@s.com	$2y$10$.FHxQXFO7c5A6tmEudx9EOLJgty2XRcf1.KEeF5Lt/Uq0D4kkfPAy	a	a		f	t
13	aasas@s.com	$2y$10$fL7V7jXPFEI7/koDOV/JNuBc8HI6OJlIcMXAH8MVanr9V/MrFffcO	s	s		t	t
14	s111@s.com	$2y$10$L/mIwWc/23.LVSgZmyQtaeDzVzDyN7/Syeml4bFV1vhxhMCvLTuNC	1	1		t	t
15	222s@s.coms	$2y$10$k8mqJOKFkvo0ZtrPktbPfepuHS0nAg55b76jsMrZIgXRlqLAx5EVi	a	s	s	t	f
16	usshu@gmail.com	$2y$10$/nJwpEcMigtkY0yh/kojDuQnxbZHDV1sVi7hRmCC2RW3LyMAOTRBS	s	s		t	t
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ulises
--

SELECT pg_catalog.setval('public.users_id_seq', 16, true);


--
-- Name: users users_correo_key; Type: CONSTRAINT; Schema: public; Owner: ulises
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_correo_key UNIQUE (correo);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: ulises
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

