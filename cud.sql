CREATE TABLE users (
    id serial primary key,
    correo character varying(255) NOT NULL,
    contrasena character varying(255) NOT NULL,
    nombre character varying(255) NOT NULL,
    primer_apellido character varying(50) NOT NULL,
    segundo_apellido character varying(50),
    habilitado boolean NOT NULL,
    administrador boolean NOT NULL,
    token character varying(50) NULL,
    fecha_token timestamp NULL
);


CREATE TABLE bitacoras (
    id serial primary key,
    id_usuario integer NOT NULL,
    accion character varying(255) NOT NULL,
    fecha timestamp NOT NULL,
    CONSTRAINT fk_bitacoras FOREIGN KEY (id_usuario) REFERENCES users (id)
);
