<?php
namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher; //include this line
use Cake\ORM\Entity;
use Cake\Mailer\Email;

/**
 * User Entity
 *
 * @property int $id
 * @property string $correo
 * @property string $contrasena
 * @property string $nombre
 * @property string $primer_apellido
 * @property string|null $segundo_apellido
 * @property bool $habilitado
 * @property bool $administrador
 */
class User extends Entity
{

    protected function _setContrasena($contrasena)
    {
        $hasher = new DefaultPasswordHasher();
        return $hasher->hash($contrasena);
    }

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'correo' => true,
        'contrasena' => true,
        'nombre' => true,
        'primer_apellido' => true,
        'segundo_apellido' => true,
        'habilitado' => true,
        'administrador' => true,
        'token' => true,
        'fecha_token' => true,
        'photo' => true,
        'photo_dir' => true
    ];

}
