<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Bitacoras Model
 *
 * @method \App\Model\Entity\Bitacora get($primaryKey, $options = [])
 * @method \App\Model\Entity\Bitacora newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Bitacora[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Bitacora|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Bitacora|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Bitacora patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Bitacora[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Bitacora findOrCreate($search, callable $callback = null, $options = [])
 */
class BitacorasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('bitacoras');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

}
