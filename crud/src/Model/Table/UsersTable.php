<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher; 

/**
 * Users Model
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Proffer.Proffer', [
            'photo' => [	// The name of your upload field
                'root' => WWW_ROOT . 'files', // Customise the root upload folder here, or omit to use the default
                'dir' => 'photo_dir',	// The name of the field to store the folder
            ]
        ]);
    }

    //Función de validación de usuarios
    public function findAuth(\Cake\ORM\Query $query, array $options)
    {
        $query
            ->select(['id', 'correo', 'contrasena'])
            ->where(['Users.habilitado' => 1]);

        return $query;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('correo')
            ->maxLength('correo', 255)
            ->requirePresence('correo', 'create')
            ->allowEmptyString('correo', false)
            ->add('correo', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('contrasena')
            ->maxLength('contrasena', 255)
            ->requirePresence('contrasena', 'create')
            ->allowEmptyString('contrasena', false);

        $validator
            ->scalar('nombre')
            ->maxLength('nombre', 255)
            ->requirePresence('nombre', 'create')
            ->allowEmptyString('nombre', false)
            ->add('nombre', 'validFormat', [
                "rule" => array('custom', '/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/i'),
                'message' => 'Solo introduce letras'
            ]);

        $validator
            ->scalar('primer_apellido')
            ->maxLength('primer_apellido', 50)
            ->requirePresence('primer_apellido', 'create')
            ->allowEmptyString('primer_apellido', false)
            ->add('primer_apellido', 'validFormat', [
                "rule" => array('custom', '/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/i'),
                'message' => 'Solo introduce letras'
            ]);

        $validator
            ->scalar('segundo_apellido')
            ->maxLength('segundo_apellido', 50)
            ->allowEmptyString('segundo_apellido')
            ->add('segundo_apellido', 'validFormat', [
                "rule" => array('custom', '/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]*$/i'),
                'message' => 'Solo introduce letras'
            ]);

        $validator
            ->boolean('habilitado')
            ->requirePresence('habilitado', 'create')
            ->allowEmptyString('habilitado', false);

        $validator
            ->boolean('administrador')
            ->requirePresence('administrador', 'create')
            ->allowEmptyString('administrador', false);

        $validator
            ->provider('proffer', 'Proffer\Moderl\Validation\ProfferRules')
            ->add('photo', 'extension', [
                "rule" => ['extension', ['jpg', 'png', 'gif']],
                'message' => 'La imagen no tiene una correcta extensión'
            ])
            ->add('photo', 'fileSize', [
                "rule" => ['fileSize', '<=', '1MB'],
                'message' => 'La imagen no debe exceder de 1MB'
            ]);

        return $validator;
    }

    
    public function validationCambiarContrasena(Validator $validator)
    {
        $validator
            ->integer('id');

        $validator
            ->scalar('contrasena_actual')
            ->add('contrasena_actual', 'custom', [
                'rule' => function ($value, $context){
                    $users = TableRegistry::get('Users');
                    $user = $users
                        ->find()
                        ->where(['id' => $context["data"]["id"]])
                        ->first();
                    return (new DefaultPasswordHasher)->check($value, $user->contrasena);
                },
                'message' => 'La contraseña ingresada no concuerda con la actual'
            ]);

        $validator
            ->scalar('nueva_contrasena')
            ->add('nueva_contrasena', 'validFormat', [
                "rule" => array('custom', '/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[@$!%*?&._])[A-Za-z\d@$!%*?&._]{8,30}$/'),
                'message' => 'La contraseña debe tener entre 8 y 30 caracteres, contener al menos una letra mayúscula, una letra minúscula, un número y un caracter especial que se encuentre en: @$!%*?&._'
            ]);

        $validator
            ->scalar('confirma_contrasena')
            ->add('confirma_contrasena', 'custom', [
                'rule' => function ($value, $context){
                    $nueva_contrasena=$context["data"]["nueva_contrasena"];
                    return $nueva_contrasena==$value;
                },
                'message' => 'La contraseña de verificación no coincide.'
            ]);

        return $validator;
    }



    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['correo']));

        return $rules;
    }
}
