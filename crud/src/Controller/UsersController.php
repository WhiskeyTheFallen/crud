<?php
namespace App\Controller;

use App\Controller\AppController;

use Cake\Mailer\MailerAwareTrait;

use Cake\Auth\DefaultPasswordHasher;

use Cake\ORM\TableRegistry;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Recaptcha.Recaptcha', [
            'enable' => true,     // true/false
            'sitekey' => '6LfE6ooUAAAAAAalJy0FjEu25yzfvV5qAe2fR9YK', 
            'secret' => '6LfE6ooUAAAAAB37_QWGtQvW2zaj0c46xU5t0ugQ',
            'type' => 'image',  // image/audio
            'theme' => 'dark', // light/dark
            'lang' => 'es',      // default en
            'size' => 'normal'  // normal/compact
        ]);
    }

    public $paginate = [
        'limit' => 10,
        'order' => [
            'Users.primer_apellido' => 'asc'
        ]
    ];

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $user = $this->Users->get($this->Auth->User('id'));
        $id= $this->Auth->User('id'); 

        if(!($user->administrador)){
            $this->viewBuilder()->template('index_usuario');
        }
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
        $this->set(compact('id')); 
    }

    private function agregarBitacora($accion)
    {
        $bitacoras = TableRegistry::get('Bitacoras');
        $bitacora=$bitacoras->newEntity();
        $bitacora->id_usuario = $this->Auth->User('id');
        $bitacora->accion = $accion;
        $bitacora->fecha = date("d-m-Y H:i:s");
        $bitacoras->save($bitacora);
       
    }

    use MailerAwareTrait;
    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function crear()
    {

        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->getMailer('User')->send('bienvenido', [$user->correo, 'Contraseña', $this->request->getData()["contrasena"]]);
                $this->Flash->success(__('Usuario guardado.'));
                $this->agregarBitacora("Registro de usuario");
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('No se pudo guardar el usuario.'));
        }
        $this->set(compact('user'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function editar($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Se edito el usuario.'));
                $this->agregarBitacora("Edición de usuario");
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('No se pudo editar el usuario.'));
        }
        $this->set(compact('user'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('El usuario ha sido eliminado.'));
        } else {
            $this->Flash->error(__('El usuario no puedo eliminarse'));
        }

        return $this->redirect(['action' => 'index']);
    }

    // Login
    public function login(){
        $this->viewBuilder()->setLayout('normal');
        if($this->request->is('post')){
            if ($this->Recaptcha->verify()) {
                $user = $this->Auth->identify();
                if($user){
                    $this->Auth->setUser($user);
                    $this->agregarBitacora("Inicio de sesión");
                    return $this->redirect(['action' => 'index']);
                }
                else{
                    $this->Flash->error('Correo o contraseña incorrectos');
                }
            }
            else{
                $this->Flash->error('Completa el Recaptcha');
            }
        }
    }

    // Logout
    public function logout(){
        $this->Flash->success('Se ha cerrado la sesión.');
        $this->agregarBitacora("Cierre de sesión");
        return $this->redirect($this->Auth->logout());
    }

    // Cambiar contraseña
    public function cambiarContrasena($id = null){

        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            
            $user = $this->Users->patchEntity($user, $this->request->getData(), ['validate' => 'cambiarContrasena']);
            $contrasena=$this->request->data['nueva_contrasena'];
            $user["contrasena"]=$contrasena;

            if ($this->Users->save($user)) {
                $this->agregarBitacora("Cambio de contraseña");
                $this->Flash->success(__('Se cambio la contraseña'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('No se pudo cambiar la contraseña'));
        }
        $this->set(compact('user'));
    }

    private function aleatorio() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); 
        $alphaLength = strlen($alphabet) - 1; 
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass);
    }

    // Cambiar contraseña
    public function recuperarContrasena($token = null){
        $this->viewBuilder()->setLayout('normal');
        if ($this->request->is('post')) {
            
            $query = $this->Users->findByCorreo($this->request->data['correo']);
            $user = $query->first();
            if (is_null($user)) {
                $this->Flash->error('Tu correo no está registrado.');
            }
            else{
                $usuario = $this->Users->get($user->id, [
                    'contain' => []
                ]);
                $token=$this->aleatorio();
                $usuario["fecha_token"]=time() + DAY; 
                $usuario["token"]=$token;
                if ($this->Users->save($usuario)) {
                    $this->getMailer('User')->send('recuperarContrasena', [$this->request->data['correo'], 'Recuperar contraseña', $token.$user->id]);
                    $this->agregarBitacora("Recuperación de contraseña");
                    $this->Flash->success(__('Ve a tu correo para poder cambiar tu contraseña.'));
                    return $this->redirect(['action' => 'login']);
                }
            }
        }
        if($token){
            $contrasena = substr($token, 0, -1);
            $id = substr($token, -1);
            $usuario = $this->Users->get($id, [
                'contain' => []
            ]);
            
            if($usuario->fecha_token >\Cake\I18n\Time::now() && $usuario->token==$contrasena){
                $usuario["contrasena"]=$contrasena;
                if ($this->Users->save($usuario)) {
                    $this->getMailer('User')->send('nuevaContrasena', [$usuario->correo, 'Nueva contraseña', $contrasena]);
                    $this->Flash->success(__('Tu nueva contraseña se ha enviado a tu correo.'));
                    return $this->redirect(['action' => 'login']);
                }
            }
            else{
                $this->Flash->error('Tu link ha caducado.');
            }
        }
    }

    public function isAuthorized($user)
    {
        if (in_array($this->request->getParam('action'), ['index', 'logout', 'cambiarContrasena'])) {
            return true;
        }

        return parent::isAuthorized($user);
    }


}
