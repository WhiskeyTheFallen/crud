<?php
namespace App\Mailer;

use Cake\Mailer\Mailer;

/**
 * User mailer.
 */
class UserMailer extends Mailer
{

    /**
     * Mailer's name.
     *
     * @var string
     */
    public static $name = 'User';

    public function bienvenido($correo, $asunto, $contrasena){

        $this
            ->profile("mensajes")
            ->template('email')
            ->emailFormat('html')
            ->viewVars(['asunto'=>$asunto, 'mensaje'=>"<h3 style='color: white;'>Hola, aquí se encuentra tu contraseña: </h3><h3 style='color: white;'>".$contrasena."</h3>"])
            ->from(['email.sender.dgtic@gmail.com' => 'CakePHP'])
            ->to($correo)
            ->subject($asunto);

    }

    public function recuperarContrasena($correo, $asunto, $token){

        $this
            ->profile("mensajes")
            ->template('email')
            ->emailFormat('html')
            ->viewVars(['asunto'=>$asunto, 'mensaje'=>"<h3 style='color: white;'>Para recuperar tu contraseña da click en el siguiente link, tienes un día antes de que se caduque:</h3><a href=http://localhost/crud/crud/users/recuperar-contrasena/".$token." style='color: yellow;'>LINK</a>"])
            ->from(['email.sender.dgtic@gmail.com' => 'CakePHP'])
            ->to($correo)
            ->subject($asunto);

    }

    public function nuevaContrasena($correo, $asunto, $contrasena){

        $this
            ->profile("mensajes")
            ->template('email')
            ->emailFormat('html')
            ->viewVars(['asunto'=>$asunto, 'mensaje'=>"<h3 style='color: white;'>Hola, aquí se encuentra tu nueva contraseña: </h3><h3 style='color: white;'>".$contrasena."</h3>"])
            ->from(['email.sender.dgtic@gmail.com' => 'CakePHP'])
            ->to($correo)
            ->subject($asunto);

    }
}
