

<div>
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend style="text-align: center;">Cambiar contraseña</legend>
        
        <?php
            echo $this->Form->control('id',["type"=>"hidden", "value"=>$user->id]);
            echo $this->Form->control('contrasena_actual', ["label"=>"Contraseña actual", "value"=>"", 'required' => true]);
            echo $this->Form->control('nueva_contrasena', ["label"=>"Nueva contraseña", 'required' => true]);
            echo $this->Form->control('confirma_contrasena', ["label"=>"Confirma contraseña", 'required' => true]); 
        ?>
    </fieldset>
    <div class="col text-center">
        <?= $this->Form->button("Enviar", array('class'=>'btn btn-primary'))?>
    </div>
    <?= $this->Form->end() ?>
    <?= $this->Html->link("<< Volver", ['action' => 'index']) ?>
</div>
