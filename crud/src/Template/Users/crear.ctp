<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */

function randomPassword() {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

?>

<div>
    <?= $this->Form->create($user, ['type'=>'file']) ?>
    <fieldset>
        <legend style="text-align: center;">Añadir usuario</legend>
        
        <?php

            $contra=randomPassword();

            echo $this->Form->control('correo',["type"=>"email"]);
            echo $this->Form->control('contrasena',["type"=>"hidden", "value"=>$contra]);
            echo $this->Form->control('nombre');
            echo $this->Form->control('primer_apellido');
            echo $this->Form->control('segundo_apellido');
            echo $this->Form->select('habilitado', [
                '1' => 'Habilitado',
                '0' => 'Deshabilitado'
            ], ['empty' => 'Estado', 'required' => true]);
            
            echo "<br><br>";

            echo $this->Form->select('administrador', [
                '1' => 'Administrador',
                '0' => 'Usuario'
            ], ['empty' => 'Tipo', 'required' => true]);
            echo "<br><br>";
            echo $this->Form->input('photo', ['type' => 'file']);
           
        ?>
    </fieldset>
    <div class="col text-center">
        <?= $this->Form->button("Enviar", array('class'=>'btn btn-primary'))?>
    </div>
    <?= $this->Form->end() ?>
    <?= $this->Html->link("<< Volver", ['action' => 'index']) ?>
</div>
