<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div >
    <?= $this->Form->create($user, ['type'=>'file']) ?>
    <fieldset>
        <legend style="text-align: center;">Editar usuario</legend>
        <?php
            echo $this->Html->image('/webroot/files/users/photo/'.$user->photo_dir.'/'.$user->photo, ["width"=>"400", "height"=>"400","style"=>"display: block; margin: 0 auto;"]);
            echo "<br>";
            echo $this->Form->control('correo',['disabled' => 'true', "type"=>"email"]);
            echo $this->Form->control('nombre');
            echo $this->Form->control('primer_apellido');
            echo $this->Form->control('segundo_apellido');
            echo $this->Form->select('habilitado', [
                '1' => 'Habilitado',
                '0' => 'Deshabilitado'
            ], ['empty' => 'Estado', 'required' => true]);

            echo "<br><br>";

            echo $this->Form->select('administrador', [
                '1' => 'Administrador',
                '0' => 'Usuario'
            ], ['empty' => 'Tipo', 'required' => true]);
            echo "<br><br>";
            echo $this->Form->input('photo', ['type' => 'file']);
           
        ?>
    </fieldset>
    <div class="col text-center">
        <?= $this->Form->button("Enviar", array('class'=>'btn btn-primary'))?>
        <?= $this->Html->link("Cambiar contraseña", ['action' => 'cambiarContrasena', $user->id], ['class' => 'btn btn-warning']) ?>
    </div>
    <?= $this->Form->end() ?>
    <?= $this->Html->link("<< Volver", ['action' => 'index']) ?>
</div>
