
<div id="login">
    <div class="container">
        <br>
        <div id="login-row" class="row justify-content-center align-items-center">
            <div id="login-column" class="col-md-6">
                <div id="login-box" class="col-md-12">
                    <?= $this->Form->create() ?>
                    <fieldset>
                        <legend style="text-align: center;">Inicio de sesión</legend>
                        
                        <?php
                            echo $this->Form->control('correo',["type"=>"email"]);
                            echo $this->Form->control('contrasena',["type"=>"password"]);

                        ?>
                    </fieldset>
                    <div class="col text-right">
                        <?= $this->Form->button("Enviar", array('class'=>'btn btn-primary'))?>
                    </div>
                    
                    <br>
                    <div style=" text-align: center;">
                         <?= $this->Recaptcha->display() ?>
                    </div> 
                    <br>
                    <div style=" text-align: center;">
                        <?= $this->Html->link("¿Olvidaste tu contraseña?", ['action' => 'recuperarContrasena', '']) ?>
                    </div>    
                    
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>
 