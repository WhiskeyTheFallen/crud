<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>
<div class="container-fluid">
    <br>
    <table class="table table-dark">
        <thead>
            <tr>
                <th colspan="7" style="text-align: center;"><?= $this->Html->link("Agregar usuario", ['action' => 'crear'], ['class' => 'btn btn-success']) ?></th>
            </tr>
            <tr>
                <th ><?= $this->Paginator->sort('correo') ?></th>
                <th scope="col"><?= $this->Paginator->sort('nombre') ?></th>
                <th scope="col"><?= $this->Paginator->sort('primer_apellido') ?></th>
                <th scope="col"><?= $this->Paginator->sort('segundo_apellido') ?></th>
                <th scope="col"><?= $this->Paginator->sort('habilitado') ?></th>
                <th scope="col"><?= $this->Paginator->sort('administrador') ?></th>
                <th style="width: 50%" scope="col">Accciones</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($users as $user): ?>
            <tr>
                <td><?= h($user->correo) ?></td>
                <td><?= h($user->nombre) ?></td>
                <td><?= h($user->primer_apellido) ?></td>
                <td><?= h($user->segundo_apellido)?h($user->segundo_apellido):"Campo vacio" ?></td>
                <td><?= h($user->habilitado)?"Habilitado":"Deshabilitdo" ?></td>
                <td><?= h($user->administrador)?"Administrador":"Usuario" ?></td>
                <td>
                    <?= $this->Html->link("Editar", ['action' => 'editar', $user->id], ['class' => 'btn btn-info']) ?>
                    <br><br>
                    <?= $this->Form->postLink("Eliminar", ['action' => 'delete', $user->id], ['class' => 'btn btn-danger', 'confirm' => __('¿Estás seguro que quieres eliminar el usuario con el correo {0}?', $user->correo)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>


    <nav aria-label="Page navigation">
        <div class="paginator">
            <ul class="pagination">
                <?php
                $this->Paginator->templates([
                    'prevActive' => '<li class="page-item"><a class="page-link" href="{{url}}">{{text}}</a></li>',
                    'prevDisabled' => '<li class="page-item disabled"><a class="page-link" href="{{url}}">{{text}}</a></li>',
                    'number' => '<li class="page-item"><a class="page-link" href="{{url}}">{{text}}</a></li>',
                    'current' => '<li class="page-item active"><a class="page-link" href="{{url}}">{{text}}</a></li>',
                    'nextActive' => '<li class="page-item"><a class="page-link" href="{{url}}">{{text}}</a></li>',
                    'nextDisabled' => '<li class="page-item disabled"><a class="page-link" href="{{url}}">{{text}}</a></li>'
                ]); ?>

                <?= $this->Paginator->first('<< Primero' )?>
                <?= $this->Paginator->prev('< Anterior') ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next('Siguiente >') ?>
                <?= $this->Paginator->last('Último >>') ?>

            </ul>
        </div>
    </nav>
</div>
