

<div>
    <?= $this->Form->create() ?>
    <fieldset>
        <legend style="text-align: center;">Introduce tu correo para enviarte un link de confirmación</legend>
        
        <?php
            echo $this->Form->control('correo', ["label"=>"Correo", 'required' => true]);
        ?>
    </fieldset>
    <div class="col text-center">
        <?= $this->Form->button("Enviar", array('class'=>'btn btn-primary'))?>
    </div>
    <?= $this->Form->end() ?>
    <?= $this->Html->link("<< Volver", ['action' => 'login']) ?>
</div>
